Gem::Specification.new do |s|
  s.name        = 'package'
  s.version     = '0.1'
  s.authors     = ['Loic Nageleisen']
  s.license     = '3BSD'
  s.email       = ['loic.nageleisen@gmail.com']
  s.homepage    = 'https://github.com/lloeki/ruby-package'
  s.summary     = 'Container implementing importable namespacing semantics'

  s.files = Dir['{lib}/**/*'] +
            ['LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'binding_of_caller'
  s.add_development_dependency 'pry'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'rake', '~> 10.3'
end
