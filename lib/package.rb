require 'binding_of_caller'
require 'package/ext'
require 'package/namespace'
require 'package/store'
require 'package/bind'
require 'package/import'
require 'package/resolve'

# Container implementing importable namespacing semantics
#
# ruby uses the Module instances to localize constants
# ruby assigns Module instances to constants within other Module instances to implement namespaces
# note: Class inherits from Module, hence behaves the same in terms of implementing namespaces
# require merely Kernel#load some file in a lazy manner, with a path resolution algorithm, and is not concerned with namespacing
# the loading mechanism being decoupled from the namespace mechanism means that:
# - the responsibility of correct loading order is on the developer
# there is no warning that a call is undefined before the call site is reached
# scenario: file A needs Date, requires 'date'. file B needs date, uses 'date', no crash.
# ... as long as A gets loaded before B (removal, autoloading)
#
# rails autoloading tries to palliate that but has to fight against ruby (see "optimistic" const resolution)
# Const dependency tracking is a beast, having a constant defined in multiple files is a recipe for surprises => always nest
# then undoes that with eager loading
# brittle, bites you back at the worst possible time, especially with engines
# errors are often weird and do not point to the problem, maybe even point in another direction
#
# it is extremely recommended to follow conventions and have a constant per file and a file per constant
# whose naming are derived from one another, imposing a bind between namespace and filesystem
# this also comes into play with gems and LOAD_PATH (both gems have a lib/ext.rb file...)
# it is then !DRY (ludicrous) to have to write the name already written in the path
# reopening a class means rewriting its inheritance
#
# each constant then has to be required
# rails autoloading tries to palliate that but has to fight against ruby (see "optimistic" const resolution)
#
# requiring therefore has to be done once, in one place, as early as possible:
# gems => lib/gem-name.rb
# app => depends?
#
# removing dependencies...
#
# should it be assigned to a constant, a package must behave just like a module
# to preserve Foo::Bar::Baz use
#
# once imported, it is accessible directly
# nested modules are made available, but their parents are not imported
# one can import select elements
class Package < Module
  class << self
    include Import
    include Store
    include Bind
    include Resolve
  end

  # Load source into the module
  def initialize(filename, namespace)
    super()
    @source_file = File.expand_path(filename)
    @name = Namespace.classify(namespace)
    @namespace = namespace
    load_in_module(filename)
  end

  attr_reader :name, :namespace, :source_file
  alias_method :to_s, :name

  # Load the file, with an option to wrap its content into the package
  #
  # default is not to wrap, to preserve require and load behavior
  def load(file, wrap = false)
    wrap ? super : load_in_module(File.join(dir, file))
  rescue Errno::ENOENT
    # Delegate error to Kernel#load
    super
  end

  def inspect
    "#<#{self.class.name} #{namespace}>"
  end

  # Evaluate the file content in the module context
  def load_in_module(filename)
    module_eval(IO.read(filename),
                File.expand_path(filename))
  rescue Errno::ENOENT
    raise LoadError, "cannot load such file -- #{filename}"
  end

  # Callback to wrap each function into the package
  def method_added(name)
    module_function(name)
  end
end
