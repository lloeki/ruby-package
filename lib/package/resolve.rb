class Package < Module
  module Resolve
    def resolve(ns)
      $LOAD_PATH.reduce(nil) do |_, path|
        candidate = File.join(path, Namespace.to_filename(ns))
        break(candidate) if File.exist?(candidate)
        nil
      end
    end
  end
end
