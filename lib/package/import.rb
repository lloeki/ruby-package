class Package < Module
  module Import
    # Import package
    #
    # as: default: last component of namespace
    # to: [:method|:const|:local|:value]
    def import(binding, *items, from: nil, as: nil, to: :value)
      from ||= items.first
      to ||= :method

      unless respond_to?("import_to_#{to}")
        fail ArgumentError, "to: #{to.inspect} unsupported"
      end

      send("import_to_#{to}", items.first, binding, as: as)
    end

    # Import package as a return value
    #
    # Does not mangle anythong and merely return the package when called.
    # Assignation for later reference is up to the caller.
    def import_to_value(namespace, _ = nil, _ = nil)
      Package.new(namespace)
    end

    # Import package as a local variable in the provided binding
    #
    # Important note: due to limitations of bindings, it is not possible
    # to define a new local variable, only set an existing one.
    def import_to_local(namespace, binding, as: nil)
      name = as || Namespace.last(namespace)
      package = import_to_value(namespace)

      bind_to_local(binding, name, package)
    end

    # Import package as an instance variable in the provided binding
    def import_to_instance(namespace, binding, as: nil)
      name = as || Namespace.last(namespace)
      package = import_to_value(namespace)

      bind_to_instance(binding, name, package)
    end

    # Import package as a method in the provided binding's self
    #
    # The method will be made private so as not to be exported to importers
    def import_to_method(namespace, binding, as: nil)
      name = as || Namespace.last(namespace)
      package = import_to_value(namespace)

      bind_to_method(binding, name, package)
    end

    # Import package as a constant in the provided binding's self
    #
    # There is no way to prevent the constant from being accessible,
    # therefore it'll be reachable from the outside.
    def import_to_const(namespace, binding, as: nil)
      name = as || Namespace.last(namespace)
      package = import_to_value(namespace)

      bind_to_const(binding, name, package)
    end
  end
end
