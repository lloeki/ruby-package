# Extend the kernel with #import
module Kernel
  # Import a module into the current namespace
  #
  # This method captures the caller's binding
  def import(namespace, upper_binding = nil, from: nil, as: nil, to: nil)
    # TODO: make gem binding_of_caller optional
    upper_binding ||= binding.of_caller(1)

    Package.import(upper_binding, namespace, from: from, as: as, to: to)
  end
end
