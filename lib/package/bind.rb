class Package < Module
  module Bind
    def bind_to_local(binding, name, value)
      sym = name.to_sym
      bound_closure = binding.eval(<<-RUBY)
        eval("#{sym} = nil")  # bind sym out of the lambda
        -> (v) { #{sym} = v } # set sym to value and return
      RUBY

      bound_closure.call(value)
    end

    def bind_to_instance(binding, name, value)
      sym = name.to_sym
      clr = binding.eval('self')
      bound_closure = clr.instance_eval(<<-RUBY)
        -> (v) { @#{sym} = v }
      RUBY

      bound_closure.call(value)
    end

    def bind_to_method(binding, name, value)
      sym = name.to_sym
      clr = binding.eval('self')
      bound_closure = clr.instance_eval(<<-RUBY)
        -> (v) do
          define_method(:#{sym}) { v }
          private(:#{sym})              # don't export to importers
          v                             # return package to caller
        end
      RUBY

      bound_closure.call(value)
    end

    def bind_to_const(binding, name, value)
      sym = name.to_s.capitalize.to_sym
      clr = binding.eval('self')
      bound_closure = clr.instance_eval(<<-RUBY)
        -> (v) { self.class.const_set(:#{sym}, v) }
      RUBY

      bound_closure.call(value)
    end
  end
end
