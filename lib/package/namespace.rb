class Package < Module
  module Namespace
    SEPARATOR = '/'
    EXTENSION = '.rb'

    module_function

    # Map a filename path to a namespace path
    def from_filename(filename)
      filename.gsub(File::SEPARATOR, SEPARATOR).gsub(/#{EXTENSION}$/, '')
    end

    # Map a namespace path to a filename path
    def to_filename(ns)
      ns.gsub(SEPARATOR, File::SEPARATOR) << EXTENSION
    end

    # Extract last component of a namespace
    def last(ns)
      ns.split(SEPARATOR).last
    end

    # Map a namespace string to a constant namespace
    def classify(namespace)
      namespace.split(SEPARATOR).map! do |v|
        v.split('_').map!(&:capitalize).join('')
      end.join('::')
    end
  end
end
