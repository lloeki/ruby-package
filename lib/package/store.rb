class Package < Module
  module Store
    # Create a new package from a namespace
    #
    # Return the existing package if it has already been loaded
    def new(ns, reload: false)
      # TODO: Fallback to ns being a filename?
      # TODO: watch out for fn / ns mix
      unless (filename = resolve(ns))
        fail LoadError, "cannot load such file -- #{ns}"
      end

      if (package = self[ns]) && !reload
        return package
      end

      self[ns] = super(filename, ns)
    end

    # Get the package instance from the namespace
    def [](ns)
      loaded[ns]
    end

    # Set the package instance associated to the namespace
    def []=(ns, value)
      loaded[ns] = value
    end

    # List files loaded and their package instance
    def loaded
      @store ||= {}
    end
  end
end
