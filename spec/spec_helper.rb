require 'package'
require 'pry'

# load support files
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

# set up load path
$LOAD_PATH.unshift(PathHelper.fixture_path('lib'))

RSpec.configure do |config|
  config.include PathHelper
end
