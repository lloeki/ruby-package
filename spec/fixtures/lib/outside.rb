import 'outside/inside'
import 'simple'

def who
  "I am #{name}"
end

def access(sym)
  "I can access #{send(sym).name}"
end
