require 'spec_helper'

describe Kernel do
  it 'should be extended with #import' do
    expect(Kernel).to respond_to :import
  end
end
