require 'spec_helper'

describe Package::Resolve do
  it 'should find a matching file in the LOAD_PATH' do
    expect(Package.resolve('empty')).to eq fixture_path('lib', 'empty.rb')
  end

  it 'should return nil when there\'s no matching file in the LOAD_PATH' do
    expect(Package.resolve('non-existent')).to be_nil
  end
end
