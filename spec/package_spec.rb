require 'spec_helper'

describe Package do
  context 'missing package file' do
    it 'should raise an error' do
      expect { Package.new('none') }.to raise_error(LoadError)
    end
  end

  context 'empty package' do
    let(:p) { Package.new('empty') }

    it 'should create a package from a namespace path' do
      expect(p).to be_a Package
      expect(p.name).to eq 'Empty'
      expect(p.namespace).to eq 'empty'
      expect(p.source_file).to eq fixture_path('lib', 'empty.rb')
    end

    it 'should return the same package again' do
      p1 = p  # force 'let' evaluation
      p2 = Package.new('empty')
      expect(p1.object_id).to eq p2.object_id
    end

    it 'should reload the package when forced to' do
      p1 = p  # force 'let' evaluation
      p2 = Package.new('empty', reload: true)
      expect(p1).not_to eq p2
    end

    it 'should reload the package when forced to' do
      p1 = p  # force 'let' evaluation
      p2 = Package.new('empty', reload: true)
      expect(p1).not_to eq p2
    end
  end

  context 'simple package' do
    let(:p) { Package.new('simple') }

    it 'should export methods' do
      expect(p).to respond_to :hello
      expect(p.hello).to eq 'Hello, world!'
    end

    it 'should export constants' do
      expect(p.const_defined?(:WORLD)).to eq true
      expect(p::WORLD).to eq 'world'
    end

    it 'should have access to metadata methods' do
      expect(p.who).to eq 'I am Simple'
    end
  end

  context 'importing package' do
    let(:p) { Package.new('outside') }

    it 'should have access to imported but not nested module' do
      expect(p.access :simple).to eq 'I can access Simple'
    end

    it 'should have access to imported and nested module' do
      expect(p.access :inside).to eq 'I can access Outside::Inside'
    end

    it 'should not expose its imported modules' do
      pending 'private not working'
      expect { p.simple }.to raise_error(NoMethodError)
      expect { p.inside }.to raise_error(NoMethodError)
    end
  end

  context 'nested' do
    let(:p) { Package.new('outside/inside') }

    it 'should be aware of its nesting' do
      expect(p.namespace).to eq 'outside/inside'
    end

    it 'should offer access to itself directly' do
      expect(p.who).to eq 'I am Outside::Inside'
    end
  end
end
