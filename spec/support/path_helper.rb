# Easily access to paths
module PathHelper
  module_function

  def fixture_path(*args)
    File.expand_path(File.join(__FILE__, '../..', 'fixtures', *args))
  end
end
